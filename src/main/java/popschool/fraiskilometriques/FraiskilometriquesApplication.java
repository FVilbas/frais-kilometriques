package popschool.fraiskilometriques;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FraiskilometriquesApplication {

    public static void main(String[] args) {
        SpringApplication.run(FraiskilometriquesApplication.class, args);
    }

}

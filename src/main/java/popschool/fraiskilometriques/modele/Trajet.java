package popschool.fraiskilometriques.modele;

/*import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import openRouteService.APIGeo;
import service.ServiceUser;*/
//import openRouteService.APIGeo;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name="trajet")
public class Trajet {
    private Long id;
    private Utilisateur utilisateur;
    private Vehicule vehicule;
    private BaremeKilo bareme;
    private double distanceKm;
    private String commentaire;
    private Date date;
    private String modalite;
    private double prix;
    private Adresse aller;
    private Adresse retour;

    public Trajet() {
    }

    public Trajet(Utilisateur utilisateur, Vehicule vehicule, BaremeKilo bareme, String commentaire, String modalite, double prix, Adresse aller, Adresse retour, Date date) {
        setUtilisateur(utilisateur);
        setVehicule(vehicule);
        setBareme(bareme);
        setCommentaire(commentaire);
        setModalite(modalite);
        setPrix(prix);
        setAller(aller);
        setRetour(retour);
        setDate(date);


        /*APIGeo api = new APIGeo();

        String coordonneesDepart = api.findCoordoneesAdresse(aller);
        String coordonneesArrivee = api.findCoordoneesAdresse(retour);

        double distance = api.demandeDistanceTrajet(coordonneesDepart,coordonneesArrivee);


        setDistanceKm(distance);*/
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "utilisateur")
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @ManyToOne
    @JoinColumn(name = "vehicule")
    public Vehicule getVehicule() {
        return vehicule;
    }
    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "bareme")
    public BaremeKilo getBareme() {
        return bareme;
    }
    public void setBareme(BaremeKilo bareme) {
        this.bareme = bareme;
    }

    @Basic
    @Column(name = "distance_km")
    public double getDistanceKm() {
        return distanceKm;
    }
    public void setDistanceKm(double distanceKm) {
        this.distanceKm = distanceKm;
    }

    /*
    public void setDistanceKm(double distanceKm) {

        List<Adresse> liste = null;
        assert false;
        liste.add(aller);
        liste.add(retour);
        JsonObject distance = null;
        try {
            distance = APIGeo.getDistanceBetweenAddresses(liste);
        } catch (Exception e) {
            e.printStackTrace();
        }

        distanceKm = distance.getJsonObject("routes").getJsonObject("summary").getDouble("distance");

        this.distanceKm = distanceKm;
    }*/

    @Basic
    @Column(name = "commentaire", length = 150)
    public String getCommentaire() {
        return commentaire;
    }
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    @Basic
    @Column(name = "date")
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "modalite", length = 20)
    public String getModalite() {
        return modalite;
    }
    public void setModalite(String modalite) {
        this.modalite = modalite;
    }

    @Basic
    @Column(name = "prix")
    public double getPrix() {
        return prix;
    }
    public void setPrix(double prix) {
        this.prix = prix;
    }

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "aller")
    public Adresse getAller() {
        return aller;
    }
    public void setAller(Adresse aller) {
        this.aller = aller;
    }

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "retour")
    public Adresse getRetour() {
        return retour;
    }
    public void setRetour(Adresse retour) {
        this.retour = retour;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trajet trajet = (Trajet) o;
        return id == trajet.id &&
                utilisateur == trajet.utilisateur &&
                vehicule == trajet.vehicule &&
                bareme == trajet.bareme &&
                Double.compare(trajet.distanceKm, distanceKm) == 0 &&
                Double.compare(trajet.prix, prix) == 0 &&
                Objects.equals(commentaire, trajet.commentaire) &&
                Objects.equals(date, trajet.date) &&
                Objects.equals(modalite, trajet.modalite);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, utilisateur, vehicule, bareme, distanceKm, commentaire, date, modalite, prix);
    }

    @Override
    public String toString() {
        return "Trajet{" +
                "id=" + id +
                ", utilisateur=" + utilisateur.getNom() +
                ", vehicule=" + vehicule.getModele() +
                ", bareme=" + bareme +
                ", distanceKm=" + distanceKm +
                ", commentaire='" + commentaire + '\'' +
                ", date=" + date +
                ", modalite='" + modalite + '\'' +
                ", prix=" + prix +
                ", aller=" + aller +
                ", retour=" + retour +
                '}';
    }
}

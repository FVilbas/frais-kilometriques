package popschool.fraiskilometriques.modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bareme_kilo")
public class BaremeKilo {
    private long id;
    private String typeVehicule;
    private int chevaux;
    private double coef;

    public BaremeKilo() {
    }

    public BaremeKilo(String typeVehicule,int chevaux,double coef) {
        setChevaux(chevaux);
        setTypeVehicule(typeVehicule);
        setCoef(coef);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type_vehicule", length = 10)
    public String getTypeVehicule() {
        return typeVehicule;
    }

    public void setTypeVehicule(String typeVehicule) {
        this.typeVehicule = typeVehicule;
    }

    @Basic
    @Column(name = "chevaux", length = 11)
    public int getChevaux() {
        return chevaux;
    }

    public void setChevaux(int chevaux) {
        this.chevaux = chevaux;
    }

    @Basic
    @Column(name = "coef")
    public double getCoef() {
        return coef;
    }

    public void setCoef(double coef) {
        this.coef = coef;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaremeKilo that = (BaremeKilo) o;
        return id == that.id &&
                chevaux == that.chevaux &&
                Double.compare(that.coef, coef) == 0 &&
                Objects.equals(typeVehicule, that.typeVehicule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, typeVehicule, chevaux, coef);
    }
}

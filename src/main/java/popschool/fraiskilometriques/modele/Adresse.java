package popschool.fraiskilometriques.modele;

/*import io.vertx.core.json.JsonObject;
import openRouteService.APIGeo;
import service.ServiceUser;
//import openRouteService.APIGeo;*/

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="adresse")

public class Adresse {

    private Long id;
    private String numero;
    private String rue;
    private String complement;
    private String codePostal;
    private String ville;
    private String pays;
    private String latitude;
    private String longitude;
    private Utilisateur utilisateur;

    @OneToOne(mappedBy = "adresse")
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Adresse)) return false;
        Adresse adresse = (Adresse) o;
        return numero.equals(adresse.numero) &&
                rue.equals(adresse.rue) &&
                codePostal.equals(adresse.codePostal) &&
                ville.equals(adresse.ville) &&
                Objects.equals(pays, adresse.pays);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numero, rue, codePostal, ville, pays);
    }

    public Adresse() {
    }

    public Adresse(String numero,String rue, String complement, String codePostal, String ville, String pays) {
        setNumero(numero);
        setRue(rue);
        setComplement(complement);
        setCodePostal(codePostal);
        setVille(ville);
        setPays(pays);

        /*APIGeo api = new APIGeo();
        String coordonnees = api.findCoordoneesAdresse(numero, rue, complement, codePostal, ville, pays);

        latitude = coordonnees.substring(coordonnees.indexOf("[")+1,coordonnees.indexOf(",")).trim(); //Latitude
        longitude = coordonnees.substring(coordonnees.indexOf(",")+1,coordonnees.indexOf("]")).trim(); //Longitude

        setLatitude(latitude);
        setLongitude(longitude);*/
    }

 /*   public static void main(String[] args) {

        ServiceUser service = ServiceUser.getSingleton();
        service.createAdresse("10", "Rue des Mouettes", null, "62600", "Berck", "France");

    }*/

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    @Basic
    @Column(name = "numero", length = 10)
    public String getNumero() { return numero; }
    public void setNumero(String numero) { this.numero = numero; }

    @Basic
    @Column(name = "rue", length = 50)
    public String getRue() { return rue; }
    public void setRue(String rue) { this.rue = rue; }

    @Basic
    @Column(name = "complement", length = 50)
    public String getComplement() { return complement; }
    public void setComplement(String complement) { this.complement = complement; }

    @Basic
    @Column(name = "code_postal", length = 10)
    public String getCodePostal() { return codePostal; }
    public void setCodePostal(String codePostal) { this.codePostal = codePostal; }

    @Basic
    @Column(name = "ville", length = 30)
    public String getVille() { return ville; }
    public void setVille(String ville) { this.ville = ville; }

    @Basic
    @Column(name = "pays", length = 30)
    public String getPays() { return pays; }
    public void setPays(String pays) { this.pays = pays; }

    @Basic
    @Column(name = "latitude", length = 60)
    public String getLatitude() { return latitude; }
    public void setLatitude(String latitude) { this.latitude = latitude; }

    @Basic
    @Column(name = "longitude", length = 60)
    public String getLongitude() { return longitude; }
    public void setLongitude(String longitude) { this.longitude = longitude; }

    @Override
    public String toString() {
        return numero + " "+ rue +", "+complement + ", "+codePostal+ " "+ville +" ("+pays+")";

    }
}
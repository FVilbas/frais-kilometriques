package popschool.fraiskilometriques.modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="vehicule")
public class Vehicule {
    private Long id;
    private Utilisateur proprietaire;
    private String constructeur;
    private String modele;
    private String categorie;
    private int chevaux;

    public Vehicule() {
    }

    public Vehicule(Utilisateur proprietaire,String constructeur,String modele,String categorie,int chevaux) {
        this.proprietaire=proprietaire;
        setConstructeur(constructeur);
        setModele(modele);
        setChevaux(chevaux);
        setCategorie(categorie);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "proprietaire")
    public Utilisateur getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Utilisateur proprietaire) {
        this.proprietaire = proprietaire;
    }

    @Basic
    @Column(name = "constructeur")
    public String getConstructeur() {
        return constructeur;
    }

    public void setConstructeur(String constructeur) {
        this.constructeur = constructeur;
    }

    @Basic
    @Column(name = "modele")
    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    @Basic
    @Column(name = "categorie")
    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Basic
    @Column(name = "chevaux")
    public int getChevaux() {
        return chevaux;
    }

    public void setChevaux(int chevaux) {
        this.chevaux = chevaux;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicule vehicule = (Vehicule) o;
        return id == vehicule.id &&
                proprietaire == vehicule.proprietaire &&
                chevaux == vehicule.chevaux &&
                Objects.equals(constructeur, vehicule.constructeur) &&
                Objects.equals(modele, vehicule.modele) &&
                Objects.equals(categorie, vehicule.categorie);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, proprietaire, constructeur, modele, categorie, chevaux);
    }

    @Override
    public String toString() {
        return "Vehicule{" +
                "proprietaire=" + proprietaire +
                ", constructeur='" + constructeur + '\'' +
                ", modele='" + modele + '\'' +
                ", categorie='" + categorie + '\'' +
                '}';
    }
}

package popschool.fraiskilometriques.modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;


@Entity
@Table(name="utilisateur")
public class Utilisateur {
    private Long id;

    // Level 1 : basic user (See own drives, add new drives)
    // Level 2 : Consult (Administration, see all drives)
    // Level 3 : Admin (see al drives + add drives)
    // Level 4 : Super Admin (Admin rights + add user + change scale drive fees)
    private int niveau;


    private String nom;
    private String prenom;
    private String email;
    private String mdp;
    private Adresse adresse;
    private Collection<Trajet> trajets;
    private Collection<Vehicule> vehicules;

    public Utilisateur() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilisateur)) return false;
        Utilisateur that = (Utilisateur) o;
        return email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    public Utilisateur(int niveau, String mdp, String nom, String prenom,
                       String email, String numero, String rue, String complement, String codePostal, String ville, String pays) {
        setNiveau(niveau);
        setMdp(mdp);
        setNom(nom);
        setPrenom(prenom);
        setEmail(email);
        this.adresse = new Adresse(numero,rue,complement,codePostal,ville,pays);
    }

    public Utilisateur(int niveau, String mdp, String nom, String prenom,
                       String email, Adresse adresse) {
        setNiveau(niveau);
        setMdp(mdp);
        setNom(nom);
        setPrenom(prenom);
        setEmail(email);
        this.adresse = adresse;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "niveau")
    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    @Basic
    @Column(name = "nom", length = 30)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom", length = 30)
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "email", length = 30, unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "mdp", length = 30)
    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    @OneToOne
    @JoinColumn(name = "adresse")
    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }


    @OneToMany(mappedBy = "utilisateur")
    public Collection<Trajet> getTrajets() {
        return trajets;
    }

    public void setTrajets(Collection<Trajet> trajets) {
        this.trajets = trajets;
    }

    @OneToMany(mappedBy = "proprietaire")
    public Collection<Vehicule> getVehicules() {
        return vehicules;
    }

    public void setVehicules(Collection<Vehicule> vehicules) {
        this.vehicules = vehicules;
    }

    @Override
    public String toString() {
        return nom.toUpperCase() + " " +prenom +'\n'+
                adresse.getNumero()+" "+adresse.getRue()+'\n'+
                adresse.getComplement()+'\n'+
                adresse.getCodePostal()+" "+adresse.getVille()+'\n'+
                adresse.getPays();
    }

    public void transfertUser (Utilisateur utilisateur){
        this.adresse=utilisateur.adresse;
        this.email=utilisateur.email;
        this.id=utilisateur.id;
        this.mdp=utilisateur.mdp;
        this.niveau=utilisateur.niveau;
        this.nom=utilisateur.nom;
        this.prenom=utilisateur.prenom;
        this.trajets=utilisateur.trajets;
        this.vehicules=utilisateur.vehicules;
    }
}

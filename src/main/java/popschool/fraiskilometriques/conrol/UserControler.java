package popschool.fraiskilometriques.conrol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.fraiskilometriques.modele.Trajet;
import popschool.fraiskilometriques.modele.Utilisateur;
import popschool.fraiskilometriques.service.UserInterface;

import java.util.Optional;

@Controller("ctrlControl")
@SessionAttributes
public class UserControler {

    @Autowired
    UserInterface service;

    @GetMapping("/userPage")
    public String getUserPage(Model model,
                              @SessionAttribute("userSession") Utilisateur user,
                              @RequestParam("page") Optional<Integer> page,
                              @RequestParam("size") Optional<Integer> size) {


        // User Session
        model.addAttribute("userSession",user);

        //First page
        int currentPage = page.orElse(1);

        //Size of age
        int pageSize = size.orElse(20);


        // Pageable, with size, first page, sort by date
        Pageable sortByDate =
                PageRequest.of(currentPage-1,pageSize, Sort.by("date"));

        // Creation of the page
        Page<Trajet> pageUser = service.findTrajetByUser(user.getId(),sortByDate);

        //Add page to attributes
        model.addAttribute("pagedrives",pageUser);



        return "userPage";
    }


}

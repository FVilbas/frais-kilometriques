package popschool.fraiskilometriques.conrol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.fraiskilometriques.modele.Utilisateur;
import popschool.fraiskilometriques.service.LoginInterface;
import popschool.fraiskilometriques.service.ServiceException;

@Controller("ctrlogin")
@SessionAttributes("userSession")
public class LoginControler {

    @Autowired
    LoginInterface service;

    // Create instance of User Session as session attribute
    @ModelAttribute("userSession")
    public Utilisateur defineUserSession(){
        return new Utilisateur();
    }


    //First page will be login
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {

        return "login";
    }

    //Login
    //If login, go to user page (accroding to user level)
    //Else, come back login (+ error message)
    @PostMapping("/connect")
    public String clientList(Model model, @RequestParam("email") String email, @RequestParam("password") String password,
                             @ModelAttribute("userSession") Utilisateur user ){


        try {

            //Try login
            //Send error for wrong password or unknown email
            //Put the user in session
            user.transfertUser(service.findUser(email,password));


            //Redirect accroding to the level of user (Admin, norma etc...)
            switch (user.getNiveau()){
                case 1 : return "redirect:/userPage";
                case 2 : return "redirect:/consultPAge";
                case 3 : return "redirect:/adminPage";
                case 4 : return "redirect:/superPage";
            }

        } catch (ServiceException e) {
            model.addAttribute("error",e.getMessage());
            e.printStackTrace();
            return "login";
        }
        return "login";
    }
}

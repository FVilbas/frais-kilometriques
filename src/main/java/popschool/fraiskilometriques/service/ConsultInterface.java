package popschool.fraiskilometriques.service;




import popschool.fraiskilometriques.modele.Trajet;
import popschool.fraiskilometriques.modele.Utilisateur;

import java.util.Date;
import java.util.List;

public interface ConsultInterface{

    List<Trajet> findTrajetByUser (int idUser);
    List<Utilisateur> findAllUser();
    Utilisateur findUserByEmail(String email);
    List<Trajet> findTrajetByUserByDate (int user, Date mindate, Date maxdate);
    Utilisateur findUserById(int id);
    void createUser(Utilisateur user);
    void updateUser(Utilisateur user);
    void deleteUSer(Utilisateur user);



}

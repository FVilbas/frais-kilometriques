package popschool.fraiskilometriques.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import popschool.fraiskilometriques.dao.TrajetDAO;
import popschool.fraiskilometriques.modele.Trajet;
import popschool.fraiskilometriques.modele.Utilisateur;


import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ServiceUser implements UserInterface{

    @Autowired
    TrajetDAO trajetDAO;


    //Page of drives for one user
    @Override
    public Page<Trajet> findTrajetByUser(Long idUser, Pageable pageable) {
        return trajetDAO.findByUtilisateur_Id(idUser, pageable);
    }

    //Page of drives for one user, between tow date
    //Default max and min date if null
    //Default max : current date
    //default min : first drive of user, or if not, 1994-01-26
    @Override
    public Page<Trajet> findTrajetByUserByDate(Long user, Date mindate, Date maxdate, Pageable pageable) {
        java.sql.Date min =(mindate==null?
                (trajetDAO.findMinDateTrajet()!=null ? trajetDAO.findMinDateTrajet() :  java.sql.Date.valueOf("2010-09-14"))
                : new java.sql.Date(mindate.getTime()));
        java.sql.Date max =(maxdate==null?new java.sql.Date(System.currentTimeMillis()):new java.sql.Date(maxdate.getTime()) );

        return trajetDAO.findByUtilisateur_IdBetweenDates(user, min,max,pageable);
    }
    

}

package popschool.fraiskilometriques.service;





import popschool.fraiskilometriques.modele.Utilisateur;

import java.util.List;

public interface LoginInterface {


    //Find a User from his email (unique) and return the user if the password is the right one
    //Throws exception message if the mail doesn't exist or if the password is not the right one
    Utilisateur findUser(String mail, String password) throws ServiceException;

}

package popschool.fraiskilometriques.service;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import popschool.fraiskilometriques.modele.Trajet;
import popschool.fraiskilometriques.modele.Utilisateur;

import java.util.Date;
import java.util.List;

public interface SuperInterface {


    //List of all users
    List<Utilisateur> findAllUser();

    //find a user from his email
    Utilisateur findUserByEmail(String email) throws ServiceException;


    //find a user from his id
    Utilisateur findUserById(Long id) throws ServiceException;


}

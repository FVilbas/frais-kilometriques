package popschool.fraiskilometriques.service;

public class ServiceException extends Exception{
    String message;

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ServiceException(String s) {
        super(s);
        switch (s){
            case "utilisateur" :
                setMessage("User not found");
                System.out.println(getMessage());
                return ;
            case "trajet" :
                setMessage("Trajet not found");
                System.out.println(getMessage());
                return;
            case "mdp" :
                setMessage("Wrong password");
                System.out.println(getMessage());
                return;

        }
    }

}

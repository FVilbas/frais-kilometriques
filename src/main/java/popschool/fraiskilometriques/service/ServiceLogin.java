package popschool.fraiskilometriques.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import popschool.fraiskilometriques.dao.UtilisateurDAO;
import popschool.fraiskilometriques.modele.Utilisateur;


import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ServiceLogin implements LoginInterface{

    @Autowired
    UtilisateurDAO utilisateurDAO;


    @Override
    public Utilisateur findUser(String mail, String password) throws ServiceException {

        Optional<Utilisateur> optionalClient= utilisateurDAO.findByEmail(mail);
        Utilisateur user;

        if(optionalClient.isPresent()){
            user=optionalClient.get();
        }else {
            throw new ServiceException("utilisateur");
        }

        if(!user.getMdp().equals(password)){
            throw new ServiceException("mdp");
        }
        return user;
    }

}

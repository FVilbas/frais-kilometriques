package popschool.fraiskilometriques.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import popschool.fraiskilometriques.dao.*;
import popschool.fraiskilometriques.modele.Trajet;
import popschool.fraiskilometriques.modele.Utilisateur;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class ServiceConsult implements ConsultInterface{

    @Autowired
    private AdresseDAO adresseDAO;

    @Autowired
    private BaremKiloDAO baremKiloDAO;

    @Autowired
    private TrajetDAO trajetDAO;

    @Autowired
    private UtilisateurDAO utilisateurDAO;

    @Autowired
    private VehiculeDAO vehiculeDAO;


    @Override
    public List<Trajet> findTrajetByUser(int idUser) {
        return null;
    }

    @Override
    public List<Utilisateur> findAllUser() {
        return null;
    }

    @Override
    public Utilisateur findUserByEmail(String email) {
        return null;
    }

    @Override
    public List<Trajet> findTrajetByUserByDate(int user, Date mindate, Date maxdate) {
        return null;
    }

    @Override
    public Utilisateur findUserById(int id) {
        return null;
    }

    @Override
    public void createUser(Utilisateur user) {

    }

    @Override
    public void updateUser(Utilisateur user) {

    }

    @Override
    public void deleteUSer(Utilisateur user) {

    }
}

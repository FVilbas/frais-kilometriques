package popschool.fraiskilometriques.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import popschool.fraiskilometriques.dao.*;
import popschool.fraiskilometriques.modele.Trajet;
import popschool.fraiskilometriques.modele.Utilisateur;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class ServiceSuper implements SuperInterface{

    @Autowired
    private AdresseDAO adresseDAO;

    @Autowired
    private BaremKiloDAO baremKiloDAO;


    @Autowired
    private TrajetDAO trajetDAO;

    @Autowired
    private UtilisateurDAO utilisateurDAO;

    @Autowired
    private VehiculeDAO vehiculeDAO;


    @Override
    public List<Utilisateur> findAllUser() {
        return (List<Utilisateur>) utilisateurDAO.findAll();
    }

    @Override
    public Utilisateur findUserByEmail(String email) throws ServiceException {
        Optional<Utilisateur> optionalUtilisateur =utilisateurDAO.findByEmail(email);
        Utilisateur utilisateur;
        if(optionalUtilisateur.isPresent()){
            utilisateur = optionalUtilisateur.get();
        }else throw new ServiceException("utilisateur");

        return utilisateur;
    }

    @Override
    public Utilisateur findUserById(Long id) throws ServiceException{
        Optional<Utilisateur> optionalUtilisateur =utilisateurDAO.findById(id);
        Utilisateur utilisateur;
        if(optionalUtilisateur.isPresent()){
            utilisateur = optionalUtilisateur.get();
        }else throw new ServiceException("utilisateur");

        return utilisateur;
    }
}

package popschool.fraiskilometriques.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import popschool.fraiskilometriques.modele.Trajet;
import popschool.fraiskilometriques.modele.Utilisateur;

import java.util.Date;
import java.util.List;

public interface UserInterface {

    //Page of drives for one user
    Page<Trajet> findTrajetByUser(Long idUser, Pageable pageable);

    //Page of drives for one user, between tow date
    //Default max and min date if null
    //Default max : current date
    //default min : first drive of user, or if not, 1994-01-26
    Page<Trajet> findTrajetByUserByDate(Long user, Date mindate, Date maxdate, Pageable pageable);
}

package popschool.fraiskilometriques.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.fraiskilometriques.modele.BaremeKilo;


public interface BaremKiloDAO extends CrudRepository<BaremeKilo,Long> {
}

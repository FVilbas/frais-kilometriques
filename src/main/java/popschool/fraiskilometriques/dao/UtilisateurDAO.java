package popschool.fraiskilometriques.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.fraiskilometriques.modele.Utilisateur;

import java.util.List;
import java.util.Optional;

public interface UtilisateurDAO extends CrudRepository<Utilisateur, Long> {

    @Query("select u from Utilisateur u where u.email= :email")
    Optional<Utilisateur> findByEmail(String email);

    @Query("select u from Utilisateur u join fetch Trajet")
    List<Utilisateur> findAllUsers();


}

package popschool.fraiskilometriques.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.fraiskilometriques.modele.Vehicule;


public interface AdresseDAO extends CrudRepository<Vehicule,Long> {


}

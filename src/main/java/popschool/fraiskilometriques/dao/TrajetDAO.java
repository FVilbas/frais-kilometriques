package popschool.fraiskilometriques.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.fraiskilometriques.modele.Trajet;

import java.sql.Date;
import java.util.List;

public interface TrajetDAO extends CrudRepository<Trajet, Long> {

    
    //Page of drives for one user
    Page<Trajet> findByUtilisateur_Id(Long id, Pageable pageable);

    //Page of drives for one user, between two dates
    @Query(value = "SELECT t FROM Trajet t WHERE t.utilisateur.id =:id and t.date>:mindate and t.date<:maxdate order by t.date")
    Page<Trajet> findByUtilisateur_IdBetweenDates(Long id, Date mindate, Date maxdate, Pageable pageable);


    @Query(value = "select min (t.date) from Trajet t ")
    Date findMinDateTrajet();

}

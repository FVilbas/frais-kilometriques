package popschool.fraiskilometriques.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.fraiskilometriques.modele.Vehicule;

import java.util.List;

public interface VehiculeDAO extends CrudRepository<Vehicule, Long> {

    List<Vehicule> findAllByProprietaire_Id(Long id);
}
